module.exports = {
  projects: [
    '<rootDir>/apps/cs425-app',
    '<rootDir>/apps/api',
    '<rootDir>/libs/broc-ui',
    '<rootDir>/libs/login',
    '<rootDir>/libs/airline-interfaces',
    '<rootDir>/libs/home',
  ],
};
